# GsonGetter Sample #

The project is an Android app, which asks Yandex.Translate API to translate an input and then shows the result.
The most interesting part is [`GsonGetter`](https://h0tk3y@bitbucket.org/h0tk3y/gson-getter-sample/src/2c45a7b59638eaeb2ed30a58dc48f342db355aa2/sample/src/main/java/ru/ifmo/ctddev/igushkin/gsongetter/GsonGetter.java?at=master) class, which downloads JSON of arbitrary structure (thanks to gson), given a class.
### How to run ###

* Open or import project as an Android-gradle project in IntelliJ IDEA or Android Studio;
* Build and run or use [APK](https://h0tk3y@bitbucket.org/h0tk3y/gson-getter-sample/src/2c45a7b59638eaeb2ed30a58dc48f342db355aa2/sample/build/outputs/apk/sample-debug.apk?at=master) which is already built.